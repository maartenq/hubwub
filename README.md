# hubwub

[![PyPI version](https://badge.fury.io/py/hubwub.svg)](https://badge.fury.io/py/hubwub)
[![Documentation Status](https://readthedocs.org/projects/hubwub/badge/?version=latest)](https://hubwub.readthedocs.io/en/latest/?badge=latest)
[![pre-commit.ci status](https://results.pre-commit.ci/badge/github/maartenq/hubwub/main.svg)](https://results.pre-commit.ci/latest/github/maartenq/hubwub/main)
[![workflow ci](https://github.com/maartenq/hubwub/actions/workflows/ci.yml/badge.svg)](https://github.com/maartenq/hubwub/actions/workflows/ci.yml)
[![codecov](https://codecov.io/gh/maartenq/hubwub/branch/main/graph/badge.svg?token=XXXXXXXXXX)](https://codecov.io/gh/maartenq/hubwub)
[![License](https://img.shields.io/badge/license-MIT%2FApache--2.0-blue.svg)](LICENSE)

## Credits

The project setup was done with the friendly assistance of [Cookiecutter] and
the [maartenq/cookiecutter-pypackage] project template.

[Cookiecutter]: https://github.com/audreyr/cookiecutter
[maartenq/cookiecutter-pypackage]: https://github.com/maartenq/cookiecutter-pypackage
