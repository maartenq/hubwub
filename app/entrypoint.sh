#!/bin/sh

PROJECT_NAME="hubwub"
APP_PORT=${PORT:-8000}

cd /app || exit

/opt/venv/bin/uvicorn \
  $PROJECT_NAME.main:app \
  --host  0.0.0.0 \
  --port "$APP_PORT"
