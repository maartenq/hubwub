# hubwub/main.py

"""main.py package for hubwub."""

import platform
from datetime import datetime
from typing import Optional

import psutil
from fastapi import FastAPI, HTTPException
from pydantic import BaseModel, Field


def get_size(bytes, suffix="B"):
    """
    Scale bytes to its proper format
    e.g:
        1253656 => '1.20MB'
        1253656678 => '1.17GB'
    """
    factor = 1024
    for unit in ["", "K", "M", "G", "T", "P"]:
        if bytes < factor:
            return f"{bytes:.2f}{unit}{suffix}"
        bytes /= factor


class SystemInfo(BaseModel):
    system: str
    node: str
    release: str
    version: str
    machine: str


class BootTime(BaseModel):
    year: int
    month: int
    day: int
    hour: int
    minute: int
    second: int


class CpuFreq(BaseModel):
    max: float
    min: float
    current: float


class CpuCore(BaseModel):
    name: str
    usage: float


class CpuInfo(BaseModel):
    physical_cores: int
    logical_cores: int
    cpu_freq: CpuFreq
    cores: list[CpuCore]


class VirtualMemory(BaseModel):
    total: int
    available: int
    used: int
    percent: float


class SwapMemory(BaseModel):
    total: int
    free: int
    used: int
    percent: float


class Memory(BaseModel):
    virtual: VirtualMemory
    swap: SwapMemory


class PartitionUsage(BaseModel):
    total_size: int
    used: int
    free: int
    percent: float


class Partition(BaseModel):
    name: str
    mount_point: str
    fstype: str
    usage: Optional[PartitionUsage] = None


def get_cpu_cores() -> list[CpuCore]:
    return [
        CpuCore(name=f"core_{i}", usage=perc)
        for i, perc in enumerate(psutil.cpu_percent(percpu=True, interval=1))
    ]


def get_cpu_info() -> CpuInfo:
    _cpufreq = psutil.cpu_freq()
    return CpuInfo(
        cpu_freq=CpuFreq(
            max=_cpufreq.max,
            min=_cpufreq.min,
            current=_cpufreq.current,
        ),
        physical_cores=psutil.cpu_count(logical=False),
        logical_cores=psutil.cpu_count(logical=True),
        cores=get_cpu_cores(),
    )


def get_partitions() -> list[Partition]:
    partitions = []
    for partition in psutil.disk_partitions():
        try:
            partition_usage = psutil.disk_usage(partition.mountpoint)
        except PermissionError:
            usage = None
        else:
            usage = PartitionUsage(
                total_size=partition_usage.total,
                used=partition_usage.used,
                free=partition_usage.free,
                percent=partition_usage.percent,
            )
        partitions.append(
            Partition(
                name=partition.device,
                mount_point=partition.mountpoint,
                fstype=partition.fstype,
                usage=usage,
            )
        )
    return partitions


class IpAddr(BaseModel):
    ip_addr: Optional[str]
    netmask: Optional[str]
    broadcast: Optional[str]


class MacAddr(BaseModel):
    mac_addr: Optional[str]
    netmask: Optional[str]
    broadcast: Optional[str]


class Interface(BaseModel):
    name: str
    ip_addrs: list[IpAddr] = Field(default_factory=list)
    mac_addrs: list[MacAddr] = Field(default_factory=list)


def get_interfaces() -> list[Interface]:
    interfaces: list[Interface] = []
    if_addrs = psutil.net_if_addrs()
    for interface_name, interface_addresses in if_addrs.items():
        interface = Interface(name=interface_name)
        for address in interface_addresses:
            if str(address.family) == "AddressFamily.AF_INET":
                interface.ip_addrs.append(
                    IpAddr(
                        ip_addr=address.address,
                        netmask=address.netmask,
                        broadcast=address.broadcast,
                    )
                )
            elif str(address.family) == "AddressFamily.AF_PACKET":
                interface.mac_addrs.append(
                    MacAddr(
                        mac_addr=address.address,
                        netmask=address.netmask,
                        broadcast=address.broadcast,
                    )
                )
        interfaces.append(interface)
    return interfaces


app = FastAPI()


@app.get("/")
async def index():
    url_list = [
        {
            "path": route.path,
            "name": route.name,
        }
        for route in app.routes
    ]
    return url_list


@app.get("/system_info")
async def system_info() -> BaseModel:
    return SystemInfo(**platform.uname()._asdict())


@app.get("/boot_time")
async def boot_time() -> BootTime:
    bt_dt = datetime.fromtimestamp(psutil.boot_time())
    return BootTime(
        year=bt_dt.year,
        month=bt_dt.month,
        day=bt_dt.day,
        hour=bt_dt.hour,
        minute=bt_dt.minute,
        second=bt_dt.second,
    )


@app.get("/cpu_info")
async def cpu_info() -> CpuInfo:
    return get_cpu_info()


@app.get("/cpu_core_info/{index}")
async def cpu_core_info(index: int) -> CpuCore:
    cpu_info = get_cpu_info()
    try:
        cpu_core = cpu_info.cores[index]
    except IndexError:
        raise HTTPException(status_code=404, detail="Item not found")
    else:
        return cpu_core


@app.get("/memory")
async def memory() -> Memory:
    vmem = psutil.virtual_memory()
    swap = psutil.swap_memory()
    memory = Memory(
        virtual=VirtualMemory(
            total=vmem.total,
            available=vmem.available,
            used=vmem.used,
            percent=vmem.percent,
        ),
        swap=SwapMemory(
            total=swap.total,
            free=swap.free,
            used=swap.used,
            percent=swap.percent,
        ),
    )
    return memory


@app.get("/partitions")
def partitions() -> list[Partition]:
    return get_partitions()


@app.get("/partition/{index}")
def partition(index: int) -> Partition:
    partitions = get_partitions()
    try:
        partition = partitions[index]
    except IndexError:
        raise HTTPException(status_code=404, detail="Item not found")
    else:
        return partition


@app.get("/interfaces")
def interfaces() -> list[Interface]:
    return get_interfaces()


@app.get("/interface/{index}")
def interface(index: int) -> Interface:
    interfaces = get_interfaces()
    try:
        interface = interfaces[index]
    except IndexError:
        raise HTTPException(status_code=404, detail="Item not found")
    else:
        return interface
