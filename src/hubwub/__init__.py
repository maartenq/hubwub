# hubwub/__init__.py

"""
Hub Wub!
"""

__version__ = "0.0.4"
__title__ = "hubwub"
__description__ = "Hub Wub!"
__uri__ = "https://github.com/maartenq/hubwub"
__author__ = "Maarten"
__email__ = "ikmaarten@gmail.com"
__license__ = "MIT or Apache License, Version 2.0"
__copyright__ = "Copyright (c) 2021 Maarten"
